package controllers

import (
	"net/http"

	"../models"
	"../utils"
	"github.com/labstack/echo"
)

var response models.Response

func PostFaq(c echo.Context) (err error) {

	u := new(models.Faq)

	if err = c.Bind(u); err != nil {
		return err
	}

	title := "[" + u.EmailAddress + "] New Message simple personal cv"
	message := "<p>Sender Name: Mr/Mrs. " + u.Name + " <br />"
	message += "Company: " + u.Company + "<br />"
	message += "</p>"
	message += "<h3>" + u.Title + "</h3>"
	message += "<p>" + u.Message + "</p>"

	_, err = utils.SendEmail(title, message)
	if err != nil {
		response.Code = http.StatusBadRequest
		response.Status = "ERROR"
		response.Message = err.Error()
	} else {
		response.Code = http.StatusOK
		response.Status = "SUCCESS"
		response.Message = ""
	}

	return c.JSON(http.StatusOK, response)
}
