package conf

import (
	"encoding/json"
	"fmt"
	"os"
)

type Conf struct {
	ConfProd struct {
		SMTPHost         string `json:"smtpHost"`
		SMTPPort         int    `json:"smtpPort"`
		SMTPEmailAddress string `json:"smtpEmailAddress"`
		SMTPPassword     string `json:"smtpPassword"`
	} `json:"confProd"`
	ConfDev struct {
		SMTPHost         string `json:"smtpHost"`
		SMTPPort         int    `json:"smtpPort"`
		SMTPEmailAddress string `json:"smtpEmailAddress"`
		SMTPPassword     string `json:"smtpPassword"`
	} `json:"confDev"`
	Receiver string `json:"receiver"`
}

type Config struct {
	SMTPHost         string
	SMTPPort         int
	SMTPEmailAddress string
	SMTPPassword     string
	Receiver         string
}

func LoadConfig() Config {

	var config Conf
	var u Config
	configFile, err := os.Open("./conf/conf.json")
	defer configFile.Close()
	if err != nil {
		fmt.Print(err.Error())
	}

	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)

	appEnv := os.Getenv("APP_ENV")

	if appEnv == "production" {
		u.SMTPHost = config.ConfProd.SMTPHost
		u.SMTPPassword = config.ConfProd.SMTPPassword
		u.SMTPEmailAddress = config.ConfProd.SMTPEmailAddress
		u.SMTPPort = config.ConfProd.SMTPPort
	} else if appEnv == "development" {
		u.SMTPHost = config.ConfDev.SMTPHost
		u.SMTPPassword = config.ConfDev.SMTPPassword
		u.SMTPEmailAddress = config.ConfDev.SMTPEmailAddress
		u.SMTPPort = config.ConfDev.SMTPPort
	}

	fmt.Printf("====================Email Config Description=================\n")
	fmt.Printf("Email: " + u.SMTPEmailAddress + "\n")
	fmt.Printf("Host: " + u.SMTPHost + "\n")
	fmt.Printf("Port: %d\n", u.SMTPPort)
	fmt.Printf("==================================================================\n")

	u.Receiver = config.Receiver

	return u
}
