package main

import (
	"net/http"
	"os"

	"../first-project/controllers"
	"../first-project/models"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

var u models.Test

/**
// practice with echo
*/
func main() {
	os.Setenv("APP_ENV", "production")
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*", "https://quirky-knuth-7a67f0.netlify.com/"},
		AllowMethods: []string{http.MethodOptions, http.MethodGet, http.MethodHead, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))
	e.Use(middleware.BodyLimit("10K"))

	e.GET("/", func(c echo.Context) error {

		u.Name = "Aji K"
		u.Address = "Tangerang"

		return c.JSON(http.StatusOK, u)
		//return c.String(http.StatusOK, "Hello, World!\n")
	})

	// FAQ
	e.POST("/api/faq", controllers.PostFaq)

	// Start server
	e.Logger.Fatal(e.Start(":1323"))
}
