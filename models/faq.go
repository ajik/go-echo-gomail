package models

type Faq struct {
	Name         string `json:"name" form:"name" query:"name"`
	EmailAddress string `json"emailAddress" form:"emailAddress" query:"emailAddress"`
	Title        string `json"title" form:"title" query:"title"`
	Company      string `json"company" form:"company"query:"company"`
	Message      string `json"message" form:"message" query:"message"`
}
