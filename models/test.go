package models

type Test struct {
	Name string `json:"name" form:"name" query:"name"`
	Address string `json"address" form:"address" query:"address"`
}