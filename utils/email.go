package utils

import (
	"fmt"

	"../conf"
	"gopkg.in/gomail.v2"
)

var emailConfig conf.Config

func SendEmail(title string, message string) (string, error) {
	mailer := gomail.NewMessage()

	emailConfig = conf.LoadConfig()

	mailer.SetHeader("From", emailConfig.SMTPEmailAddress)
	mailer.SetHeader("To", emailConfig.Receiver)
	mailer.SetHeader("Subject", title)
	mailer.SetBody("text/html", message)

	dialer := gomail.NewDialer(
		emailConfig.SMTPHost,
		emailConfig.SMTPPort,
		emailConfig.SMTPEmailAddress,
		emailConfig.SMTPPassword,
	)

	err := dialer.DialAndSend(mailer)
	if err != nil {
		fmt.Print(err.Error())
	}

	return "", err
}
