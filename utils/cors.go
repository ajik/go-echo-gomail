package utils

import (
	"net/http"

	"github.com/labstack/echo"

	"github.com/labstack/echo/middleware"
)

// HandlingCORS ...
func HandlingCORS(e echo.Echo) {

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"https://localhost:3000", "https://quirky-knuth-7a67f0.netlify.com/"},
		AllowMethods: []string{http.MethodOptions, http.MethodGet, http.MethodHead, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))
}
